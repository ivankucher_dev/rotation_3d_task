package com.epam.trainings.main;

import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.controller.ViewControllerImpl;
import com.epam.trainings.mvc.model.Menu;
import com.epam.trainings.mvc.view.View;

import java.util.Vector;


public class Main {
  public static void main(String[] args) {
    Vector vec = new Vector();
    for(Object d : vec){
      System.out.println(d);
    }
    Menu menu = new Menu();
    View view = new View(menu);
    ViewController controller = new ViewControllerImpl(view);
    view.startBySettingUpController(controller);
  }
}

package com.epam.trainings.mvc.model;

import com.epam.trainings.mvc.commands.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class Menu {
  private Map<Integer, CommandElement> menu;
  private static Logger log = LogManager.getLogger(Menu.class.getName());

  public Menu(Integer menuNum, String comName, Command firstCommand) {
    menu = new HashMap<>();
    menu.put(menuNum, new CommandElement(comName, firstCommand));
  }

  public Menu() {
    menu = new HashMap<>();
  }

  public Map<Integer, String> getMenuAsString() {
    Map<Integer, String> map = new HashMap<>();
    menu.forEach(
        (k, v) -> {
          map.put(k, v.getCommandName());
        });
    return map;
  }

  public Map<Integer, CommandElement> getMenu() {
    return menu;
  }

  public int size() {
    return menu.size();
  }

  public void add(Integer menuIndex, String comName, Command command) {
    if (menu.containsKey(menuIndex)) {
      log.warn(
          "Menu already contain ["
              + menuIndex
              + "] key , this call will refactor your previous menu command to new ");
    } else {
      menu.put(menuIndex, new CommandElement(comName, command));
    }
  }

  public Command getCommand(int menuNum) {
    if (menu.containsKey(menuNum)) {
      return menu.get(menuNum).getCommand();
    } else {
      return new Command() {
        @Override
        public void execute() {
          System.out.println("Command not found");
        }
      };
    }
  }

  public String getCommandName(int index) {
    if (menu.containsKey(index)) {
      return menu.get(index).getCommandName();
    } else {
      return "command not found";
    }
  }
}

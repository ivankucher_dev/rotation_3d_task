package com.epam.trainings.mvc.model;

import java.util.Vector;

public class VRot {
  private Vector<Double> vec;

  public VRot(Vector vec) {
    this.vec = vec;
  }

  public double getX() {
    return vec.get(0);
  }

  public double getY() {
    return vec.get(1);
  }

  public double getZ() {
    return vec.get(2);
  }
}

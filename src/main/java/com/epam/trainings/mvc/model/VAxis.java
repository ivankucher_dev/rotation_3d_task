package com.epam.trainings.mvc.model;

import java.util.Vector;

public class VAxis {

  private Vector<Double> axis;

  public VAxis(Vector<Double> axis) {
    this.axis = axis;
  }

  public double getX() {
    return axis.get(0);
  }

  public double getY() {
    return axis.get(1);
  }

  public double getZ() {
    return axis.get(2);
  }
}

package com.epam.trainings.mvc.view;

import com.epam.trainings.mvc.commands.RotationCommand;
import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.model.Menu;
import com.epam.trainings.utils.PropertiesReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class View {

  private Menu menu;
  private ViewController controller;
  private Scanner scanner;
  private static Logger log = LogManager.getLogger(View.class.getName());

  public View(Menu menu) {
    this.menu = menu;
    scanner = new Scanner(System.in);
  }

  public void startBySettingUpController(ViewController controller) {
    this.controller = controller;
    createMenu();
    show();
  }

  public void updateView() {
    show();
  }

  private void show() {
    menu.getMenuAsString().forEach((k, v) -> log.info(k + "." + v));
    int index = -1;
    try {
      index = scanner.nextInt();
    } catch (InputMismatchException e) {
      log.error(e);
      show();
    }
    controller.execute(menu.getCommand(index));
  }

  private void createMenu() {
    menu.add(1, PropertiesReader.getProperty("rotation_about_3_d"), new RotationCommand());
  }
}

package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.VAxis;
import com.epam.trainings.mvc.model.VRot;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class RotationCommand implements Command {
  private static int THREE_D_DIMENSION = 3;
  private static double MAX_DEGREES = 3.141592653;
  private static double MIN_DEGREES = 0;
  private VRot vrot;
  private Vector<Double> directions;
  private Vector vectorRot;
  private Vector vectorAxis;
  private VAxis axis;
  private double theta;
  private Scanner sc;

  public RotationCommand() {
    sc = new Scanner(System.in);
  }

  @Override
  public void execute() {
    init();
    vrot = new VRot(vectorRot);
    axis = new VAxis(vectorAxis);
    Vector result = rotateVectorCC(vrot, axis, theta);
    printResult(result);
  }

  private void init() {
    vectorRot = new Vector();
    vectorAxis = new Vector();
    directions = new Vector();
    scanData();
  }

  private void scanData() {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < THREE_D_DIMENSION; j++) {
        if (i == 0) {
          System.out.println("Enter vector element : ");
          vectorRot.add(sc.nextDouble());
        } else if (i == 1) {
          System.out.println("Enter vector axis element : ");
          vectorAxis.add(sc.nextDouble());
        } else if (i == 2) {
          System.out.println("Enter direction vector axis element : ");
          directions.add(sc.nextDouble());
        }
      }
    }
    System.out.println("Enter angle : ");
    theta = sc.nextDouble();
    while (theta > MAX_DEGREES && theta < MIN_DEGREES) {
      theta = sc.nextDouble();
    }
  }

  private void printResult(Vector result) {
    for (int i = 0; i < result.size(); i++) {
      System.out.println(result.get(i));
    }
  }

  public Vector rotateVectorCC(VRot vec, VAxis axis, double theta) {
    double x, y, z;
    double u, v, w;
    x = vec.getX();
    y = vec.getY();
    z = vec.getZ();
    u = axis.getX();
    v = axis.getY();
    w = axis.getZ();
    double xPrime =
        u * (u * x + v * y + w * z) * (1d - Math.cos(theta))
            + x * Math.cos(theta)
            + (-w * y + v * z) * Math.sin(theta);
    double yPrime =
        v * (u * x + v * y + w * z) * (1d - Math.cos(theta))
            + y * Math.cos(theta)
            + (w * x - u * z) * Math.sin(theta);
    double zPrime =
        w * (u * x + v * y + w * z) * (1d - Math.cos(theta))
            + z * Math.cos(theta)
            + (-v * x + u * y) * Math.sin(theta);
    if (directions.get(0) == 1.0) {
      xPrime = -xPrime;
    }
    if (directions.get(1) == 1.0) {
      yPrime = -yPrime;
    }
    if (directions.get(2) == 1.0) {
      zPrime = -zPrime;
    }
    ArrayList<Double> result = new ArrayList<>();
    result.add(xPrime);
    result.add(yPrime);
    result.add(zPrime);
    return new Vector(result);
  }
}
